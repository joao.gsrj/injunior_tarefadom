
let notaN=0;
let lista=[];




function adicionar() {
    let entrada = document.querySelector(".inputNota");
    if(entrada.value===""){
        alert("Por favor, insira uma nota.")
    }
    else if(isNaN(entrada.value)||entrada.value>10||entrada.value<0){
        alert("A nota digitada é inválida, por favor, insira uma nota válida.")
    }else{
    notaN+=1;
    lista.push(+entrada.value)

    
    let posicao = document.createElement("div");
    posicao.classList.add("blocoMsg");
    let msgNota = document.createElement("p");
    
    
    
    msgNota.innerText = "A nota "+notaN+" é "+entrada.value+".";
    

    let historico = document.querySelector(".historico");
    posicao.append(msgNota);
    historico.append(posicao);
   
    entrada.value="";

    
    }
}

function media(arr){
    let soma=0;
    for (i=0; i<arr.length;i++){
        soma+=arr[i]
    }
    return((soma/arr.length).toFixed(2))
}

function insereMedia(){
    if(isNaN(media(lista))){    
    }else{
    let txtMedia = document.querySelector("#txtMedia");
    txtMedia.innerText = "A média é: "+ media(lista); 
}}



let botaoAdd = document.querySelector("#botaoAdd");
botaoAdd.addEventListener("click",()=>(adicionar()));

let botaoMedia = document.querySelector("#botaoMedia");
botaoMedia.addEventListener("click",()=>(insereMedia()));



